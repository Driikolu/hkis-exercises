Provide a script that computes, then prints the first prime number
greater than 100000000.


## Advice

You may use `itertools.count()`, and you may need `sys.exit()`.
