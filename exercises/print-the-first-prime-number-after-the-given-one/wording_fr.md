Provide a script that computes, then prints the first prime number
greater than 100000000.

```bash
$ python3 solution.py
666
```

Yes, 666 is not the right answer :-)


## Advice

You may use `itertools.count()`, and you may need `sys.exit()`.
